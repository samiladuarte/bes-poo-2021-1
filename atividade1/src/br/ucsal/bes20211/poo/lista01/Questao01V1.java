package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Questao01V1 {
	/*
	 * Suponha que o conceito de um aluno seja determinado em fun��o da sua nota.
	 * Suponha, tamb�m, que esta nota seja um valor inteiro na faixa de 0 a 100
	 * (intervalo fechado), conforme a seguinte faixa: Crie um programa em Java que
	 * leia a nota de um aluno e apresente o conceito do mesmo. N�o � necess�rio
	 * tratar valores fora da faixa. 
	 * Nota Conceito
	 * 0 a 49 Insuficiente 
	 * 50 a 64 Regular 
	 * 65 a 84 Bom 
	 * 85 a 100 �timo
	 */
	static Scanner etd = new Scanner(System.in);

	public static void main(String[] args) { // ctrl+1 pra o ecl sugerir solu��es
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		//Importante definir bem os passos 
		//REFATORA��O: depois que o c�digo funciona, consiste no processo de melhorar o c�digo
		//Declara��o de vari�veis
		int nota;
		String conceito;

		//Entrada de dados
		System.out.println("Informe a nota (0 a 100, intervalo fechado): ");
		nota = etd.nextInt();

		//Processamento 
		if (nota <= 49) {
			conceito = "Insuficiente.";
		} else if (nota <= 64) {
			conceito = "Regular."; 
		} else if (nota <= 84) {
			conceito ="Bom.";
		} else {
			conceito = "�timo.";
		}
		//Sa�da
		System.out.println("O conceito para a nota " + nota + " � " +conceito);

	}
}
