package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;
/*Passos importantes pra programar, primeiro programe o que tem que fazer, pense os passos, depois foque em cada m�todo/passo esquecendo os outros e pimba*/
/* Alt + shift + t = para extrair m�todo, seleciona o que quer colocar no m�todo e d� o comando e o nome*/

public class Questao01V2 {

	static Scanner etd = new Scanner(System.in);

	public static void main(String[] args) {
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		int nota;
		String conceito;
		nota = obterNota();
		conceito = calcularConceito(nota);

		exibirConceito(nota, conceito);

	}

	private static void exibirConceito(int nota, String conceito) {
		System.out.println("O conceito para a nota " + nota + " � " + conceito);
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente.";
		} else if (nota <= 64) {
			conceito = "Regular.";
		} else if (nota <= 84) {
			conceito = "Bom.";
		} else {
			conceito = "�timo.";
		}
		return conceito;
	}

	// TODO quando eu coloco esse termo "todo" eu t� querendo dizer que algo ainda
	// precisa ser feito no c�digo
	// FIXME quando preciso corrigir algo
	private static int obterNota() {
		int nota;
		while (true) {
			System.out.println("Informe a nota (0 a 100, intervalo fechado): ");
			nota = etd.nextInt();
			if (nota >= 0 && nota <= 100) {
				return nota;
			} else {
				System.out.println("Nota fora da faixa :(");
				System.out.println("");
			}

		}
	}
}
